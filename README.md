# MJC simplon

Notre groupe de trois a décidé de partir sur un projet de MJC (Maison des Jeunes et de la Culture) qui proposerait des activités à la fois à des enfants et des adultes. Nous avons décidé de réaliser un use case diagram sur StarUml, que l'on peut trouver sur notre gitlab dans le dossier ressources. A l'issue de ce processus, nous avons fait ensemble notre diagramme de classes, que nous avons mis dans le même dossier. Nous avons aussi réalisé des maquettes fonctionnelles sur Figma, visible grâce à ce lien : https://www.figma.com/file/WKwIGb7DbR6SAJ4MpfHSQL/projet-group?node-id=0%3A1&t=20DVYbDFRokhwMJy-1

Nous avons créé un grand nombre de classes, dont certaines avaient le type many to many (activity et teachers, activity et type), ou encore du many to one (profile et user, subsrciption et prodile,...). Notre idée était de séparer les profils des habitants (profile), qui s'inscrivent aux activités des profils de profs (teachers) qui animent des activités car ils n'ont pas les même droits, mais plus tard nous nous sommes dits que nous aurions pu les mettre dans la même classe. Nous avons fait le choix de séparer les user (compte utilisateur) des profile (profil utilisateur) car nous avons pensé aux adultes qui inscrivent des enfants qui sont trop jeunes pour avoir leur propre compte. Ainsi, le compte peut être familial et permettre de gérer les activités à la MJC des différents membres d'une même famille.

Nous avons mis en place les différentes classes, avec des getters, setters et constructors dans les Entities. Nous avons aussi créé les Repositories correspondants. Enfin nous avons créé notre base de donnée grâce à un script et nous avons entré des données dedans.

Nous nous sommes réparti le travail pour le CRUD en donnant à chacun⸱e des Repositoties sur lesquels travailler (deux ou trois par personne). A partir de notre use case diagram, nous avons défini les fonctions qui sortaient du CRUD de base que nous pouvions mettre en place sur nos Repositories. Nous avons créé des issues sur nos boards GitLab et attribué les tâches qui restaient. Seules deux issues que nous n'avions pas estimées prioritaire n'ont pas été réalisées faute de temps.

Si nous devions nous améliorer sur ce genre de projet, nous pourrions penser plus à utiliser plus tôt et plus régulièrement les boards de GitLab, et estimer le temps de différentes tâches à faire.

Ci-dessous, les consignes du projet : 

## Étapes du projet

Identifier et lister sous forme de user stories ou de diagramme de use case les fonctionnalités de l'application
(Optionnel) Créer les maquettes fonctionnelles de quelques écrans de l'application
Identifier et schématiser sous forme de diagramme de classe les entités qui persisteront en base de données pour que ces fonctionnalités soient possibles
Créer un ou des scripts SQL qui permettront de mettre en place la base de données, ils contiendront la création des tables et l'insertion d'un jeu de données
Dans un projet Java, créer les classes représentant les entités et les composant d'accès aux données en utilisant JDBC

## Organisation conseillée

- La phase de conception, d'identification des fonctionnalités et des classes est bien à faire de manière collégiale, sur un pc ou sur papier en faisant en sorte que tout le monde puisse s'exprimer
- Créer dès le début un projet Gitlab afin de pouvoir utiliser les outils de ce dernier : les issues pour lister les fonctionnalités et tâche à réaliser
- Réaliser les diagrammes soit sur papier, soit sur staruml, ou bien sur draw.io ou lucidchart (mais ces deux derniers sont un peu moins agréables à utiliser)
- Lorsque vous commencez à travailler sur des tâches différentes, commencer chaque après midi par un standing meeting afin que chaque membre du groupe sache ses propres tâches et celles des autres
- Créer des scripts SQL séparés pour chaque table+insertion de données afin de plus facilement vous répartir le travail
