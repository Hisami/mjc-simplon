<?php
use App\Repository\ContactRepository;
use App\Entities\Contact;
use App\Repository\ActivityRepository;
use App\Entities\Activity;
use App\Repository\RoomRepository;
use App\Entities\Room;
use App\Repository\ProfileRepository;
use App\Repository\Profile;
use App\Repository\TeacherRepository;
use App\Entities\Teacher;
use App\Repository\SubscriptionRepository;
use App\Repository\Subscription;
use App\Repository\UserRepository;
use App\Entities\User;
use App\Repository\TypeRepository;
use App\Entities\Type;


require('../vendor/autoload.php');

/* $teachers = new TeacherRepository();

var_dump($teachers->findActivityByteacher($teachers->findById(3))); */

$activities = new ActivityRepository();
$myActivity = $activities->findById(1);

$types= new TypeRepository();
$myType = $types->findById(2);

$activities->deleteTypeOfActivity($myActivity, $myType);
var_dump($activities->findTypeByActivity($myActivity));