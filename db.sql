-- Active: 1673947702029@@127.0.0.1@3306@mjc_simplon

--DROP DATABASE IF EXISTS mjc_simplon;

DROP TABLE IF EXISTS activity_teacher;
DROP TABLE IF EXISTS activity_type;
DROP TABLE IF EXISTS subscription;
DROP TABLE IF EXISTS activity;
DROP TABLE IF EXISTS type;
DROP TABLE IF EXISTS teacher;
DROP TABLE IF EXISTS profile;
DROP TABLE IF EXISTS user;
DROP TABLE IF EXISTS contact;
DROP TABLE IF EXISTS room;


CREATE TABLE room (
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name VARCHAR(100),
    capacity INT
);

INSERT INTO room (name, capacity) VALUES 
("Atelier Paula", 12),
("Gymnase Sapin", 30),
("Salle Bleue", 10),
("Salle Concerto", 8);

CREATE TABLE contact (
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    mail VARCHAR(255),
    phoneNumber VARCHAR(255),
    street VARCHAR(255),
    ZIPCode VARCHAR(255),
    city VARCHAR(255)
);

INSERT INTO contact (mail, phoneNumber, street, ZIPCode, city) VALUES 
("marie.dupont@wanadoo.fr","0619273048","4 rue des Lilas","69278","St-Basil-le-haut"),
("benoit.faure@laposte.fr","0619275976","7 rue des Orangers","69278","St-Basil-le-haut"),
("julie.blanc@outlook.fr","0619270619","25 rue des Pruniers","69278","St-Basil-le-bas"),
("aminabensaid@orange.fr","0619275413","1 rue des Amandiers","69278","St-Basil-le-bas"),
("julie.blanc@outlook.fr","0619270619","25 rue des Pruniers","69278","St-Basil-le-bas"),
('jp.flexible@free.fr',"0619278754","131 avenue Faure","69002","Lyon"),
('philippe.peinture@free.fr',"0619279952","33 rue des Magnolias","69278","St-Basil-le-haut"),
('sylvie.casserole@protonmail.fr',"0619275516","54 chemin du Puis","69742","Villefranche"),
("jb.sketteur@hotmail.com","0619274197","10 rue des Cerisiers","69278","St-Basil-le-bas");

CREATE TABLE user (
    id INT PRIMARY KEY AUTO_INCREMENT,
    username VARCHAR(300),
    password VARCHAR(50),
    role VARCHAR(100)
);

INSERT INTO user (username, password, role) VALUES 
("marie.dupont@wanadoo.fr","sapin","member"),
("benoit.faure@laposte.fr","007jamesbond","member"),
("julie.blanc@outlook.fr","titanic","member"),
("aminabensaid@orange.fr","gâteau57","member"),
("liliane.duval@gmail.com","4321","admin"),
('jp.flexible@free.fr',"flex-man","teacher"),
('philippe.peinture@free.fr',"jaimelapeinture","teacher"),
('sylvie.casserole@protonmail.fr','kwizine',"teacher"),
("jb.sketteur@hotmail.com","michaeljordan","teacher");

CREATE TABLE profile (
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(255),
    firstname VARCHAR(255),
    birthdate DATE,
    id_user int,
    id_contact int,
    Foreign Key (id_user) REFERENCES user(id) ON DELETE CASCADE,
    Foreign Key (id_contact) REFERENCES contact(id) ON DELETE CASCADE
);

INSERT INTO profile (name, firstname, birthdate, id_user, id_contact) VALUES ("Dupont", "Marie", "1980-06-08", 1, 1),("Faure","Benoît","1985-10-26", 2, 2),("Blanc","Julie","1978-12-01", 3,3),("Bensaïd","Amina","2020-02-17", 4, 4),("Damien","Perez", "2013-07-30", 3, 5);

CREATE TABLE teacher(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name VARCHAR (100),
    firstname VARCHAR (100),
    id_contact int,
    id_user int,
    Foreign Key (id_contact) REFERENCES contact(id)ON DELETE CASCADE,
    Foreign Key (id_user) REFERENCES user(id)ON DELETE CASCADE
);

INSERT INTO teacher(firstname, name, id_contact, id_user) VALUES 
("Jean-Paul", "Flexible", 6, 6),
("Philippe", "Peinture", 7, 7),
("Sylvie", "Casserole", 8, 8),
("Jean-Ba", "Sketteur", 9, 9);

CREATE TABLE type(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    type VARCHAR(100)
);
INSERT INTO type (type) VALUES 
("Music"),
("Activités sportives"),
("Langues"),
("Arts Plastiques et Expressions"),
("Arts du spectacle"),
("Danses"),
("Jeux et jeux d’esprit"),
("Mise en forme");

CREATE TABLE activity(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name VARCHAR(100),
    dateStart DATETIME,
    dateEnd DATETIME,
    frequency int,
    nbOfSession int,
    ageMin int,
    ageMax int,
    price int,
    maxStudent int,
    pitch VARCHAR(500),
    id_room int,
    Foreign Key (id_room) REFERENCES room(id) ON DELETE CASCADE,
    level VARCHAR(10),
    img  VARCHAR(300)
);

INSERT INTO activity (name, dateStart, dateEnd, frequency, nbOfSession, ageMin, ageMax, price, maxStudent, pitch, id_room, level, img) VALUES 
('Peinture','2023-02-01','2023-02-01', 7, 32, 6, 12, 35, 15, "Apprenez à peindre à l'aquarelle et à la gouache sur différents supports." ,1 , "débutant", "/image/nomimage.jpg"),
('Yoga','2023-02-01','2023-02-01', 7, 29, 18, 99, 28, 25, "Découvrez le yoga pour vous relaxer au quotidien." ,2 , "débutant", "/image/nomimage.jpg"),
('Football','2023-02-01','2023-02-01', 7, 40, 11, 15, 46, 24, "Sport d'équipe, match et défis : inscrivez-vous au foot pour vous dépenser.", 2 , "débutant", "/image/nomimage.jpg"),
('Cuisine','2023-02-01','2023-02-01', 7, 20, 10, 14, 31, 8, "Pour mettre les petits plats dans les grands en apprenant.", 3 , "débutant", "/image/nomimage.jpg");

CREATE TABLE activity_type (
    id INT PRIMARY KEY AUTO_INCREMENT,
    id_activity INT,
    id_type INT,
    Foreign Key (id_activity) REFERENCES activity(id)ON DELETE CASCADE,
    Foreign Key (id_type) REFERENCES type(id)ON DELETE CASCADE
);

INSERT INTO activity_type (id_activity, id_type) VALUES (1,4),(2,8),(3,2),(4,4),(1,7),(2,2),(3,7),(4,6);

CREATE TABLE activity_teacher (
    id INT PRIMARY KEY AUTO_INCREMENT,
    id_activity INT,
    id_teacher INT,
    Foreign Key (id_activity) REFERENCES activity(id) ON DELETE CASCADE,
    Foreign Key (id_teacher) REFERENCES teacher(id)ON DELETE CASCADE
);

INSERT INTO activity_teacher (id_activity,id_teacher) VALUES (1,2),(2,1),(3,4),(4,3);

CREATE TABLE subscription (
    id INT PRIMARY KEY AUTO_INCREMENT,
    date DATE,
    pending BOOL,
    id_profile INT,
    id_activity INT,
    Foreign Key (id_profile) REFERENCES profile(id) ON DELETE CASCADE,
    Foreign Key (id_activity) REFERENCES activity(id) ON DELETE CASCADE
);

INSERT INTO subscription (date, pending, id_profile, id_activity) VALUES ("2023-01-30", 0, 2, 3),("2023-01-23", 0, 3, 1),("2023-01-23", 0, 3, 2);


/*DESSOUS : UNIQUEMENT DES TESTS DE SELECTIONS*/

/*Permet d'afficher les teachers des activités*/
SELECT teacher.name, activity.name FROM activity_teacher ate
JOIN activity ON ate.id_activity = activity.id 
JOIN teacher ON ate.id_teacher = teacher.id;

/*Permet d'afficher les types des activités*/
SELECT activity.name, type.type FROM activity_type at
JOIN activity ON at.id_activity = activity.id 
JOIN type ON at.id_type = type.id;

/*Pareil mais affiche toutes les données des deux tableaux*/
SELECT * FROM activity_type at
JOIN activity ON at.id_activity = activity.id 
JOIN type ON at.id_type = type.id
WHERE type.id = 2;

SELECT * FROM activity
LEFT JOIN activity_type at ON activity.id = at.id_activity
WHERE type.type = "Langues";

/*On affiche toutes les activités dans une salle qui a une capacité de 10 personnes*/
SELECT * FROM activity join room on activity.id_room = room.id WHERE room.capacity = 10;
SELECT * FROM activity_teacher;
SELECT * FROM teacher;
SELECT * FROM room;
SELECT * FROM activity;

SELECT * FROM type;

SELECT * FROM profile LEFT JOIN contact ON contact.id = profile.id WHERE profile.id = :id;

SELECT *, subscription.id sub_id FROM activity 
LEFT JOIN subscription ON activity.id = subscription.id_activity 
WHERE subscription.id_profile = :id AND subscription.pending = 0;

select * from type;
select * from activity_type;

/*retourne les activités par types*/
select type.id, activity.name
from type
left join activity_type
on type.id=activity_type.id_type
left join activity
on activity.id=activity_type.id_activity
where type.id=8;


/*retourne les types par activités*/
select activity.name, type.id
from activity 
left join activity_type
on activity.id=activity_type.id_activity
left join type
on type.id=activity_type.id_type
where activity.name='Peinture';

/*retourne les activités par prof*/
select teacher.firstname,teacher.name, activity.name
from activity
left join activity_teacher
on activity.id=activity_teacher.id_activity
left join teacher
on teacher.id=activity_teacher.id_teacher
where teacher.name='Flexible';

/*retourne les profs par activités*/
select activity.name, teacher.firstname, teacher.name
from teacher
left join activity_teacher
on teacher.id=activity_teacher.id_teacher
left join activity
on activity.id=activity_teacher.id_activity
where activity.name='Yoga';









