<?php
namespace App\Entities;

class Type{
    private ?int $id;
    private string $type;

	/**
	 * Summary of __construct
	 * @param string $type
	 * @param ?int $id
	 */
    public function __construct(string $type, ?int $id = null) {
    $this->id = $id;
    $this->type = $type;
    }

	/**
	 * @return int
	 */
	public function getId(): int {
		return $this->id;
	}
	
	/**
	 * @param int $id 
	 * @return self
	 */
	public function setId(int $id): self {
		$this->id = $id;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getType(): string {
		return $this->type;
	}
	
	/**
	 * @param string $type 
	 * @return self
	 */
	public function setType(string $type): self {
		$this->type = $type;
		return $this;
	}
}