<?php

namespace App\Entities;

use DateTime;
class Activity{
    private ?int $id;
    private string $name;
    private DateTime $dateStart;
    private DateTime $dateEnd;
    private int $frequency;
    private int $nbOfSession;
    private int $ageMin;
    private int $ageMax;
    private int $price;
    private int $maxStudent;
    private string $pitch;
    private Room $room;
	private string $level;
	private string $img;

	private int $numberOfparticipants;

	private int $totalSales;
	/**
	 * @return int|null
	 */
	public function getId(): ?int  {
		return $this->id;
	}
	
	/**
	 * @param int|null $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getName(): string {
		return $this->name;
	}
	
	/**
	 * @param string $name 
	 * @return self
	 */
	public function setName(string $name): self {
		$this->name = $name;
		return $this;
	}
	
	/**
	 * @return DateTime
	 */
	public function getDateStart(): DateTime {
		return $this->dateStart;
	}
	
	/**
	 * @param DateTime $dateStart 
	 * @return self
	 */
	public function setDateStart(DateTime $dateStart): self {
		$this->dateStart = $dateStart;
		return $this;
	}
	
	/**
	 * @return DateTime
	 */
	public function getDateEnd(): DateTime {
		return $this->dateEnd;
	}
	
	/**
	 * @param DateTime $dateEnd 
	 * @return self
	 */
	public function setDateEnd(DateTime $dateEnd): self {
		$this->dateEnd = $dateEnd;
		return $this;
	}
	
	/**
	 * @return int
	 */
	public function getFrequency(): int {
		return $this->frequency;
	}
	
	/**
	 * @param int $frequency 
	 * @return self
	 */
	public function setFrequency(int $frequency): self {
		$this->frequency = $frequency;
		return $this;
	}
	
	/**
	 * @return int
	 */
	public function getNbOfSession(): int {
		return $this->nbOfSession;
	}
	
	/**
	 * @param int $nbOfSession 
	 * @return self
	 */
	public function setNbOfSession(int $nbOfSession): self {
		$this->nbOfSession = $nbOfSession;
		return $this;
	}
	
	/**
	 * @return int
	 */
	public function getAgeMin(): int {
		return $this->ageMin;
	}
	
	/**
	 * @param int $ageMin 
	 * @return self
	 */
	public function setAgeMin(int $ageMin): self {
		$this->ageMin = $ageMin;
		return $this;
	}
	
	/**
	 * @return int
	 */
	public function getAgeMax(): int {
		return $this->ageMax;
	}
	
	/**
	 * @param int $ageMax 
	 * @return self
	 */
	public function setAgeMax(int $ageMax): self {
		$this->ageMax = $ageMax;
		return $this;
	}
	
	/**
	 * @return int
	 */
	public function getPrice(): int {
		return $this->price;
	}
	
	/**
	 * @param int $price 
	 * @return self
	 */
	public function setPrice(int $price): self {
		$this->price = $price;
		return $this;
	}
	
	/**
	 * @return int
	 */
	public function getMaxStudent(): int {
		return $this->maxStudent;
	}
	
	/**
	 * @param int $maxStudent 
	 * @return self
	 */
	public function setMaxStudent(int $maxStudent): self {
		$this->maxStudent = $maxStudent;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getPitch(): string {
		return $this->pitch;
	}
	
	/**
	 * @param string $pitch 
	 * @return self
	 */
	public function setPitch(string $pitch): self {
		$this->pitch = $pitch;
		return $this;
	}


	/**
	 * @return string
	 */
	public function getLevel(): string {
		return $this->level;
	}
	
	/**
	 * @param string $level 
	 * @return self
	 */
	public function setLevel(string $level): self {
		$this->level = $level;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getImg(): string {
		return $this->img;
	}
	
	/**
	 * @param string $img 
	 * @return self
	 */
	public function setImg(string $img): self {
		$this->img = $img;
		return $this;
	}

	/**
	 * @param int|null $id
	 * @param string $name
	 * @param DateTime $dateStart
	 * @param DateTime $dateEnd
	 * @param int $frequency
	 * @param int $nbOfSession
	 * @param int $ageMin
	 * @param int $ageMax
	 * @param int $price
	 * @param int $maxStudent
	 * @param string $pitch
	 * @param string $level
	 * @param string $img
	 */
	public function __construct(string $name, DateTime $dateStart, DateTime $dateEnd, int $frequency, int $nbOfSession, int $ageMin, int $ageMax, int $price, int $maxStudent, string $pitch, string $level, string $img,?int $id = null) {
		$this->id = $id;
		$this->name = $name;
		$this->dateStart = $dateStart;
		$this->dateEnd = $dateEnd;
		$this->frequency = $frequency;
		$this->nbOfSession = $nbOfSession;
		$this->ageMin = $ageMin;
		$this->ageMax = $ageMax;
		$this->price = $price;
		$this->maxStudent = $maxStudent;
		$this->pitch = $pitch;
		$this->level = $level;
		$this->img = $img;
	}

	/**
	 * @return Room
	 */
	public function getRoom(): Room {
		return $this->room;
	}
	
	/**
	 * @param Room $room 
	 * @return self
	 */
	public function setRoom(Room $room): self {
		$this->room = $room;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getNumberOfparticipants(): int {
		return $this->numberOfparticipants;
	}
	
	/**
	 * @param int $numberOfparticipants 
	 * @return self
	 */
	public function setNumberOfparticipants(int $numberOfparticipants): self {
		$this->numberOfparticipants = $numberOfparticipants;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getTotalSales(): int {
		return $this->totalSales;
	}
	
	/**
	 * @param int $totalSales 
	 * @return self
	 */
	public function setTotalSales(int $totalSales): self {
		$this->totalSales = $totalSales;
		return $this;
	}
}