<?php

namespace App\Entities;

class Room {
    private ?int $id;
    private string $name;
    private int $capacity;

    /**
     * @param ?int $id
     * @param string $name
     * @param ?int $capacity
     */
    public function __construct(string $name, int $capacity, ?int $id = null) {
    
    $this->name = $name;
    $this->capacity = $capacity;
    $this->id = $id;
    }

	/**
	 * @return int
	 */
	public function getId(): int {
		return $this->id;
	}
	
	/**
	 * @param int $id 
	 * @return self
	 */
	public function setId(int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getName(): string {
		return $this->name;
	}
	
	/**
	 * @param string $name 
	 * @return self
	 */
	public function setName(string $name): self {
		$this->name = $name;
		return $this;
	}
	
	/**
	 * @return int
	 */
	public function getCapacity(): int {
		return $this->capacity;
	}
	
	/**
	 * @param int $capacity 
	 * @return self
	 */
	public function setCapacity(int $capacity): self {
		$this->capacity = $capacity;
		return $this;
	}
}