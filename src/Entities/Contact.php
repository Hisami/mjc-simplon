<?php

namespace App\Entities;

class Contact {
   
    private ?int $id;

    private string $mail;

    private string $phoneNumber;

	private string $street;

    private string $ZIPCode;

    private string $city;

    /**
     * @param string $mail
     * @param string $phoneNumber
	 * @param string $street
     * @param string $ZIPCode
     * @param string $city
     * @param ?int $id
     */
    public function __construct(string $mail, string $phoneNumber, string $street, string $ZIPCode, string $city, ?int $id = null) {
    	$this->mail = $mail;
    	$this->phoneNumber = $phoneNumber;    	
		$this->street = $street;
    	$this->ZIPCode = $ZIPCode;
    	$this->city = $city;
        $this->id = $id;
    }

	/**
	 * @return int
	 */
	public function getId(): int {
		return $this->id;
	}
	
	/**
	 * @param int $id 
	 * @return self
	 */
	public function setId(int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getMail(): string {
		return $this->mail;
	}
	
	/**
	 * @param string $mail 
	 * @return self
	 */
	public function setMail(string $mail): self {
		$this->mail = $mail;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getPhoneNumber(): string {
		return $this->phoneNumber;
	}
	
	/**
	 * @param string $phoneNumber 
	 * @return self
	 */
	public function setPhoneNumber(string $phoneNumber): self {
		$this->phoneNumber = $phoneNumber;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getStreet(): string {
		return $this->street;
	}
	
	/**
	 * @param string $street 
	 * @return self
	 */
	public function setStreet(string $street): self {
		$this->street = $street;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getZIPCode(): string {
		return $this->ZIPCode;
	}
	
	/**
	 * @param string $ZIPCode 
	 * @return self
	 */
	public function setZIPCode(string $ZIPCode): self {
		$this->ZIPCode = $ZIPCode;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getCity(): string {
		return $this->city;
	}
	
	/**
	 * @param string $city 
	 * @return self
	 */
	public function setCity(string $city): self {
		$this->city = $city;
		return $this;
	}
}