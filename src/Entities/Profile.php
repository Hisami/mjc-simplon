<?php

namespace App\Entities;
use DateTime;

class Profile {

    private ?int $id;

    private string $name;

    private string $firstname;

    private DateTime $birthdate;

    private User $user;

    private Contact $contact;


    /**
     * @param string $firstname
     * @param DateTime $birthdate
     * @param ?int $id
     */
    public function __construct(string $name, string $firstname, DateTime $birthdate, ?int $id = null) {
    $this->name = $name;
    $this->firstname = $firstname;
    $this->birthdate = $birthdate;
        $this->id = $id;
    }

	/**
	 * @return int
	 */
	public function getId(): int {
		return $this->id;
	}
	
	/**
	 * @param int $id 
	 * @return self
	 */
	public function setId(int $id): self {
		$this->id = $id;
		return $this;
	}

    /**
	 * @return string
	 */
	public function getName(): string {
		return $this->name;
	}
	
	/**
	 * @param string $name 
	 * @return self
	 */
	public function setName(string $name): self {
		$this->name = $name;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getFirstname(): string {
		return $this->firstname;
	}
	
	/**
	 * @param string $firstname 
	 * @return self
	 */
	public function setFirstname(string $firstname): self {
		$this->firstname = $firstname;
		return $this;
	}
	
	/**
	 * @return DateTime
	 */
	public function getBirthdate(): DateTime {
		return $this->birthdate;
	}
	
	/**
	 * @param DateTime $birthdate 
	 * @return self
	 */
	public function setBirthdate(DateTime $birthdate): self {
		$this->birthdate = $birthdate;
		return $this;
	}
	
	

	/**
	 * @return User
	 */
	public function getUser(): User {
		return $this->user;
	}
	
	/**
	 * @param User $user 
	 * @return self
	 */
	public function setUser(User $user): self {
		$this->user = $user;
		return $this;
	}

	/**
	 * @return Contact
	 */
	public function getContact(): Contact {
		return $this->contact;
	}
	
	/**
	 * @param Contact $contact 
	 * @return self
	 */
	public function setContact(Contact $contact): self {
		$this->contact = $contact;
		return $this;
	}
}
