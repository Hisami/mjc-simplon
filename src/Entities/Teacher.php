<?php

namespace App\Entities;

class Teacher
{
    /**
	 * Summary of __construct
	 * @param string $name
	 * @param string $firstname
	 * @param ?int|null $id_contact
	 * @param ?int|null $id
	 */
    private ?int $id;
    private ?int $id_contact;
    private string $name;
    private string $firstname;

    public function __construct(string $firstname, string $name, ?int $id_contact=null, ?int $id=null)
    {
        $this->id = $id;
        $this->firstname = $firstname;
        $this->name = $name;
        $this->id_contact = $id_contact;
    }


	/**
	 * Summary of __construct
	 * @return int
	 */
	public function getId(): int {
		return $this->id;
	}
	
	/**
	 * Summary of __construct
	 * @param int $id Summary of __construct
	 * @return self
	 */
	public function setId(int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getName(): string {
		return $this->name;
	}
	
	/**
	 * @param string $name 
	 * @return self
	 */
	public function setName(string $name): self {
		$this->name = $name;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getFirstname(): string {
		return $this->firstname;
	}
	
	/**
	 * @param string $firstname 
	 * @return self
	 */
	public function setFirstname(string $firstname): self {
		$this->firstname = $firstname;
		return $this;
	}

	/**
	 * @return int|null
	 */
	public function getId_contact(): ?int {
		return $this->id_contact;
	}

	/**
	 * @param int|null $id_contact 
	 * @return self
	 */
	public function setId_contact(?int $id_contact): self {
		$this->id_contact = $id_contact;
		return $this;
	}
}
