<?php
namespace App\Entities;

class User {

    private ?int $id;
    private string $username;
    private string $password;
    private string $role;

	/**
	 * @param int|null $id
	 * @param string $username
	 * @param string $password
	 * @param string $role
	 */
	public function __construct(string $username, string $password, string $role, ?int $id = null) {
		$this->username = $username;
		$this->password = $password;
		$this->role = $role;
		$this->id = $id;
	}

	/**
	 * @return int|null
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param int|null $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getUsername(): string {
		return $this->username;
	}
	
	/**
	 * @param string $username 
	 * @return self
	 */
	public function setUsername(string $username): self {
		$this->username = $username;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getPassword(): string {
		return $this->password;
	}
	
	/**
	 * @param string $password 
	 * @return self
	 */
	public function setPassword(string $password): self {
		$this->password = $password;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getRole(): string {
		return $this->role;
	}
	
	/**
	 * @param string $role 
	 * @return self
	 */
	public function setRole(string $role): self {
		$this->role = $role;
		return $this;
	}
	
}