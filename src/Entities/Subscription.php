<?php

namespace App\Entities;

use DateTime;
class Subscription {
    private ?int $id;
    private DateTime $date;
	private bool $pending;

	private Profile $profile;

	private Activity $activity;
	/**
	 * 
	 * @param DateTime $date
	 * @param ?bool $pending
	 * @param int|null $id
	 */
	public function __construct(DateTime $date,?bool $pending=true,?int $id = null) {
		$this->date = $date;
		$this->pending = $pending;
		$this->id = $id;
	}

	/**
	 * @return int|null
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param int|null $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}

	/**
	 * @return DateTime
	 */
	public function getDate(): DateTime {
		return $this->date;
	}
	
	/**
	 * @param DateTime $date 
	 * @return self
	 */
	public function setDate(DateTime $date): self {
		$this->date = $date;
		return $this;
	}

	/**
	 * @return bool
	 */
	public function getPending(): bool {
		return $this->pending;
	}
	
	/**
	 * @param bool $pending 
	 * @return self
	 */
	public function setPending(bool $pending): self {
		$this->pending = $pending;
		return $this;
	}

	public function changePending():void{
		$this->pending = !$this->pending;
	}


	/**
	 * @return Profile
	 */
	public function getProfile(): Profile {
		return $this->profile;
	}
	
	/**
	 * @param Profile $profile 
	 * @return self
	 */
	public function setProfile(Profile $profile): self {
		$this->profile = $profile;
		return $this;
	}

	/**
	 * @return Activity
	 */
	public function getActivity(): Activity {
		return $this->activity;
	}
	
	/**
	 * @param Activity $activity 
	 * @return self
	 */
	public function setActivity(Activity $activity): self {
		$this->activity = $activity;
		return $this;
	}
}