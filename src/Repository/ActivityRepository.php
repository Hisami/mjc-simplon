<?php

namespace App\Repository;

use App\Entities\Profile;
use App\Entities\Subscription;
use PDO;
use App\Entities\Activity;
use App\Entities\Room;
use App\Entities\Teacher;
use App\Entities\Type;
use DateTime;


class ActivityRepository
{
    private PDO $connection;

    public function __construct()
    {
        $this->connection = new PDO('mysql:host=localhost;dbname=mjc_simplon', 'simplon', '1234');
    }
/**
     * Méthode qui prend un ligne de résultat PDO et la convertit en instance 
     * de Activity
     * @param array $line la ligne de résultat à convertir, sous forme de tableau associatif
     * @return Activity l'instance de activity
     */

    private function sqlToActivity(array $line):Activity {
        return new Activity($line["ac_name"], new DateTime($line["dateStart"]), new DateTime($line["dateEnd"]), $line["frequency"], $line["nbOfSession"], $line["ageMin"], $line["ageMax"], $line["price"], $line["maxStudent"], $line["pitch"], $line["level"], $line["img"], $line["ac_id"]);
    }
/**
     * Méthode qui prend un ligne de résultat PDO et la convertit en instance 
     * de Profile
     * @param array $line la ligne de résultat à convertir, sous forme de tableau associatif
     * @return Profile l'instance de profile
     */


    private function sqlToProfile(array $line): Profile
    {
    return new Profile($line["pro_name"], $line["firstname"], new DateTime($line["birthdate"]), $line["pro_id"]);
    }


/**
     * Faire une requête SQL vers la base de données et convertir les résultats de
     * cette requêtes en instances de la classe/entité Activity
     * @return Activity[]
     */
    public function findAll()
    {
        $activityArray = [];
        $statement = $this->connection->prepare('SELECT *,ac.name ac_name,ac.id ac_id,r.name rname, r.id rid FROM activity ac JOIN room r ON ac.id_room = r.id');
        $statement->execute();
        $results = $statement->fetchAll();
        foreach ($results as $line) {
            $newactivity = $this->sqlToActivity($line);
            $newactivity->setRoom(new Room($line["rname"], $line["capacity"], $line["rid"]));
            $activityArray[] = $newactivity;
        }
        return $activityArray;
    }

/**
     * Méthode permettant de faire persister une instance de Activity sur la base de données
     * @param Activity $activity l'activity à faire persister
     * @return void Aucun retour, mais une fois persisté, l'activity aura un id assigné
     */

    public function persist(Activity $activity)
    {
        $statement = $this->connection->prepare('INSERT INTO activity (name,dateStart,dateEnd,frequency,nbOfSession,ageMin,ageMax,price,maxStudent,pitch,id_room,level,img) VALUES (:name, :dateStart, :dateEnd, :frequency,:nbOfSession,:ageMin,:ageMax,:price,:maxStudent, :pitch,:id_room, :level,:img)');
        $statement->bindValue('name', $activity->getName());
        $statement->bindValue('dateStart', $activity->getDateStart()->format('Y-m-d'));
        $statement->bindValue('dateEnd', $activity->getDateEnd()->format('Y-m-d'));
        $statement->bindValue('frequency', $activity->getFrequency());
        $statement->bindValue('nbOfSession', $activity->getNbOfSession());
        $statement->bindValue('ageMin', $activity->getAgeMin());
        $statement->bindValue('ageMax', $activity->getAgeMax());
        $statement->bindValue('price', $activity->getPrice());
        $statement->bindValue('maxStudent', $activity->getMaxStudent());
        $statement->bindValue('pitch', $activity->getPitch());
        $statement->bindValue('id_room', $activity->getRoom()->getId());
        $statement->bindValue('level', $activity->getLevel());
        $statement->bindValue('img', $activity->getImg());

        $statement->execute();

        $activity->setId($this->connection->lastInsertId());


    }

/**
     * Méthode qui effectue une requête SQL vers la base de donnée pour trouve un activity spécifique à partir de son id et qui le renvoit comme instance de la classe Activity.
     * @param int $id L'id du activity à récupérer
     * @return Activity l'activity récupéré, instance de la classe Activity
     */
    public function findById(int $id):?Activity {
        $statement = $this->connection->prepare('SELECT *,ac.name ac_name,ac.id ac_id, r.name rname, r.id rid FROM activity ac JOIN room r ON ac.id_room = r.id WHERE ac.id=:id');
        $statement->bindValue('id', $id);
        $statement->execute();
        $result = $statement->fetch();
        if($result) {
            $activityfound = $this->sqlToActivity($result);
            $activityfound->setRoom(new Room($result["rname"], $result["capacity"], $result["rid"]));
        }
        return $activityfound;
    }
    
/**
     * Permet de modifier un activity dans la base de donnée en lui fournissant une nouvelle instance de Activity avec le bon id
     * @param Activity $activity le nouveau activity que l'on veut
     * @return void
     */
    public function update(Activity $activity): void
    {
        $statement = $this->connection->prepare("UPDATE activity SET name=:name, dateStart=:dateStart,dateEnd=:dateEnd,frequency=:frequency,nbOfSession=:nbOfSession,ageMin=:ageMin,ageMax=:ageMax,price=:price,maxStudent=:maxStudent,pitch=:pitch,
        level=:level,img=:img WHERE id=:id");
        $statement->bindValue("name", $activity->getName(), PDO::PARAM_STR);
        $statement->bindValue('dateStart', $activity->getDateStart()->format('Y-m-d'));
        $statement->bindValue('dateEnd', $activity->getDateEnd()->format('Y-m-d'));
        $statement->bindValue('frequency', $activity->getFrequency(), PDO::PARAM_INT);
        $statement->bindValue('nbOfSession', $activity->getNbOfSession(), PDO::PARAM_INT);
        $statement->bindValue('ageMin', $activity->getAgeMin(), PDO::PARAM_INT);
        $statement->bindValue('ageMax', $activity->getAgeMax(), PDO::PARAM_INT);
        $statement->bindValue('price', $activity->getPrice(), PDO::PARAM_INT);
        $statement->bindValue('maxStudent', $activity->getMaxStudent(), PDO::PARAM_INT);
        $statement->bindValue('pitch', $activity->getPitch(), PDO::PARAM_STR);
        $statement->bindValue('level', $activity->getLevel(), PDO::PARAM_INT);
        $statement->bindValue('img', $activity->getImg(), PDO::PARAM_STR);
        $statement->bindValue("id", $activity->getId(), PDO::PARAM_INT);

        $statement->execute();
    }

/**
     * Méthode qui permet de supprimer un activity de la base de donnée à partir de son id
     * @param int $id, l'id du activity à supprimer
     * @return $result
     */

    public function delete(int $id)
    { 
        $statement = $this->connection->prepare("DELETE activity
        FROM activity JOIN room ON activity.id_room = room.id 
        WHERE activity.id =:id");
        $statement->bindValue(":id", $id, PDO::PARAM_INT);
        $result= $statement->execute();
        
        return $result;
    }

/**
     * Permet d'afficher toutes les activités d'un profil
     * @param int $id l'id du profil dont on veut trouver les activités
     * @return array tableau d'activités
     */
    public function findActivitiesByProfile(Profile $profile):array{
        $array = [];

        $statement = $this->connection->prepare('SELECT *, subscription.id sub_id FROM activity LEFT JOIN subscription ON activity.id = subscription.id_activity WHERE subscription.id_profile = :id AND subscription.pending = 0');

        $statement->bindValue('id', $profile->getId(), PDO::PARAM_INT);

        $statement->execute();

        $results = $statement->fetchAll();

        foreach($results as $line){
            $activity = new Activity($line["name"], new DateTime($line["dateStart"]), new DateTime($line["dateEnd"]), $line["frequency"], $line["nbOfSession"], $line["ageMin"], $line["ageMax"], $line["price"], $line["maxStudent"], $line["pitch"], $line["level"], $line["img"], $line["id"]);
            $array[] = $activity;
        }
        return $array;
    }

/**
     * Permet d'ajouter un type à une activité dans la base de donnée
     * @param Activity $activity l'activité à laquelle on veut ajouter un type
     * @param Type $type le type qu'on veut rajouter
     * @return void
         */    
        public function persistType(Activity $activity, Type $type){
            $statement = $this->connection->prepare('INSERT INTO activity_type (id_activity, id_type) VALUES (:id_activity, :id_type)');
            $statement->bindValue('id_activity', $activity->getId(), PDO::PARAM_INT);
            $statement->bindValue('id_type', $type->getId(), PDO::PARAM_INT);
            $statement->execute();
        }

/**
     * Permet de trouver tous les prof (teachers) qui animent une activité
     * @param Activity $activity l'activité dont on veut trouver les profs
     * @return array renvoit un tableau de profs
         */    
        public function findTeacherByActivity(Activity $activity):array
        {
            $array = [];
            $statement = $this->connection->prepare('SELECT *, teacher.id ti
            FROM activity_teacher 
            LEFT JOIN teacher 
            ON teacher.id = activity_teacher.id_teacher
            WHERE activity_teacher.id_activity = :id');
    
            $statement->bindValue('id', $activity->getId(), PDO::PARAM_INT);
            $statement->execute();
            
            $results = $statement->fetchAll();
            foreach($results as $line){
                $teacher = new Teacher($line["name"], $line['firstname'], $line['id_contact'], $line['ti']);
                $array[] = $teacher;
            }
            return $array;
        }

/**
     * Permet de trouver les types d'une activité
     * @param Activity $activity l'activité dont on veut trouver les types
     * @return array renvoit un tableau de types
     */
    public function findTypeByActivity(Activity $activity):array
    {
        $array = [];
        $statement = $this->connection->prepare('SELECT *, type.id t_id FROM activity_type 
        LEFT JOIN type 
        ON type.id=activity_type.id_type 
        WHERE activity_type.id_activity = :id');

        $statement->bindValue('id', $activity->getId(), PDO::PARAM_INT);
        $statement->execute();
        
        $results = $statement->fetchAll();
        foreach($results as $line){
            $activity = new Type($line['type'],$line['t_id']);
            $array[] = $activity;
        }
        return $array;
    }

/**
     * Permet d'afficher toutes les subscriptions d'un activité
     * @param Activity $activity l'instance du activité dont on veut trouver les subscription
     * @return array tableau de subscriptions
     */
public function findSubscriptionByActivity(Activity $activity){
    $array = [];
    $statement = $this->connection->prepare('SELECT *, subscription.id sub_id, profile.name pro_name,profile.id pro_id, activity.name ac_name,activity.id ac_id, profile.id pro_id FROM subscription 
    LEFT JOIN profile ON subscription.id_profile =profile.id
    LEFT JOIN activity ON subscription.id_activity = activity.id
    WHERE activity.id=:id');

    $statement->bindValue('id', $activity->getId());
    $statement->execute();
    
    $results = $statement->fetchAll();
    foreach($results as $line){
        $subscription = new Subscription( new DateTime($line["date"]), $line["pending"],$line["sub_id"]);
            $subscription->setProfile( $this->sqlToProfile($line));
        $array[] = $subscription;
    }
    return $array;
}

/**
     * Permet d'afficher toutes les profiles(inscrits) d'un activité
     * @param Activity $activity l'instance du activité dont on veut trouver les profiles
     * @return array tableau de profiles
     */
public function findProfilesByActivity(Activity $activity){
        $array = [];
        $statement = $this->connection->prepare('SELECT *, subscription.id sub_id, profile.name pro_name,profile.id pro_id, activity.name ac_name,activity.id ac_id, profile.id pro_id FROM subscription 
        LEFT JOIN profile ON subscription.id_profile =profile.id
        LEFT JOIN activity ON subscription.id_activity = activity.id
        WHERE activity.id=:id HAVING pending=0');

        $statement->bindValue('id', $activity->getId());
        $statement->execute();
        
        $results = $statement->fetchAll();
        foreach($results as $line){
            $profile = new Profile($line["pro_name"], $line["firstname"], new DateTime($line["birthdate"]), $line["pro_id"]);
            $array[] = $profile;
        }
        return $array;
    }

/**
     * Permet de supprimer le type d'une activitié
     * @param Activity $activity l'activité dont on veut supprimmer un des types
     * @param Type $type le type que l'on veut supprimer de l'activité
     * @return void
 */
public function deleteTypeOfActivity(Activity $activity, Type $type){
        $statement = $this->connection->prepare('DELETE FROM activity_type WHERE id_activity = :id_activity AND id_type = :id_type');
        $statement->bindValue('id_activity', $activity->getId(), PDO::PARAM_INT);
        $statement->bindValue('id_type', $type->getId(), PDO::PARAM_INT);
        $statement->execute();
}

/**
     * Permet d'ajouter le nombre de participants et le totalSales de l'activity
     * @param Activity $activity l'instance du activité 
     * @return $totalSales de l'activité
     */
public function TotalSalesbyActivity(Activity $activity){
    $statement = $this->connection->prepare('SELECT *, COUNT("sub_id") numberptc, activity.price*COUNT("sub_id") total,subscription.id sub_id,  activity.name ac_name,activity.id ac_id FROM subscription 
    LEFT JOIN profile ON subscription.id_profile =profile.id
    LEFT JOIN activity ON subscription.id_activity = activity.id
    WHERE subscription.pending=0 AND activity.id=:id;');
    $statement->bindValue('id', $activity->getId());
    $statement->execute();
    $result=$statement->fetch();
    if($result) {
        $activity = $this->sqlToActivity($result);
            $activity->setNumberOfparticipants($result["numberptc"]);
            $activity->setTotalSales($result["total"]);
    }
        return $activity->getTotalSales();
}
    
}
