<?php

namespace App\Repository;
use App\Entities\Room;
use App\Entities\Teacher;
use App\Entities\Activity;
use DateTime;
use PDO;


class TeacherRepository{
    private PDO $connection;
    public function __construct() {
        $this->connection = new PDO('mysql:host=localhost;dbname=mjc_simplon', 'simplon', '1234');
    }

    /**
     * Renvoit tous les profs de la table teacher
     * @return array renvoit un tableau de teachers de la classe Teacher
     */
    public function findAll(): array
    {
        $teachers = [];
        $statement = $this->connection->prepare('SELECT * FROM teacher');
        $statement->execute();
        $results = $statement->fetchAll();
        foreach ($results as $item) {
            $teachers[] = new Teacher($item['firstname'],$item['name'], $item['id_contact'], $item['id']);
        }
        return $teachers;
    }

    /**
     * Permet de trouver un teacher grâce à son id
     * @param int $id l'id du teacher que l'on veut trouver
     * @return Teacher|null Renvoit un teacher
     */
    public function findById(int $id):?Teacher {
        $statement = $this->connection->prepare('SELECT * FROM teacher WHERE id=:id');
        $statement->bindValue('id', $id);
        $statement->execute();
        $result = $statement->fetch();
        if($result) {
            return new Teacher($result['name'],$result['firstname'], $result['id_contact'], $result['id']);
        }
        return null;
    }

    /**
     * Permet de faire persister un nouveau Teacher dans la base de données
     * @param Teacher $teacher le nouveau prof que l'on veut ajouter à la base de données
     * @return void
     */
    public function persist(Teacher $teacher) {
        $statement = $this->connection->prepare('INSERT INTO teacher (firstname,name) VALUES (:firstname, :name)');

        $statement->bindValue('firstname', $teacher->getFirstname());
        $statement->bindValue('name', $teacher->getName());
        $statement->execute();
    }
    /**
     * Supprime un Teacher de la base de donnée à partir de son id
     * @param int $id l'id du Teacher que l'on veut supprimer
     * @return void
     */
    public function deleteTeacherById(int $id){
        
        $statement = $this->connection->prepare('DELETE FROM teacher WHERE id=:id');
        $statement->bindValue('id', $id, PDO::PARAM_INT);
        $statement->execute();
        
    }

    /**
     * Permet de mettre à jour un teacher à partir de son id
     * @param string $firstname le prénom du Teacher à mettre à jour
     * @param string $name le nom du Teacher à mettre à jour
     * @param int $id l'id du Teacher à mettre à jour
     * @return void
     */
    public function updateTeacherById(string $firstname, string $name, int $id){
        $statement = $this->connection->prepare("UPDATE teacher SET firstname=:firstname, name=:name WHERE id=:id");
        $statement->bindValue('firstname', $firstname, PDO::PARAM_STR);
        $statement->bindValue('name', $name,PDO::PARAM_STR);
        $statement->bindValue('id', $id, PDO::PARAM_INT);
        $statement->execute();
    }

    

   /**
     * Permet de trouver les activités animées par un teacher en particulier
     * @param Teacher $teacher le prof dont on cherche les activités
     * @return array renvoit un tableau d'activités
     */
    public function findActivityByteacher(Teacher $teacher):array
    {
        $array = [];
        $statement = $this->connection->prepare('SELECT *,activity.name a_name, activity.id a_id, teacher.firstname t_fistname,teacher.name t_name, room.name r_name, room.id r_id
        FROM activity 
        LEFT JOIN activity_teacher
        ON activity.id = activity_teacher.id_activity 
        LEFT JOIN room
        ON room.id = activity.id_room
        LEFT JOIN teacher
        ON teacher.id=activity_teacher.id_teacher WHERE teacher.id=:id');

        $statement->bindValue('id', $teacher->getId(), PDO::PARAM_INT);
        $statement->execute();
        
        $results = $statement->fetchAll();
        foreach($results as $line){
            $activity = new Activity($line["a_name"], new DateTime($line["dateStart"]), new DateTime($line["dateEnd"]), $line["frequency"], $line["nbOfSession"], $line["ageMin"], $line["ageMax"], $line["price"], $line["maxStudent"], $line["pitch"], $line["level"], $line["img"], $line["a_id"]);
            $activity->setRoom(new Room($line["r_name"], $line["capacity"], $line["r_id"]));
            $array[] = $activity;
        }
        return $array;
    }

}




