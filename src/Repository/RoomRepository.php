<?php

namespace App\Repository;
use App\Entities\Room;
use PDO;

class RoomRepository
{
  private PDO $connection;

  public function __construct()
  {
    $this->connection = new PDO('mysql:host=localhost;dbname=mjc_simplon', 'simplon', '1234');
  }

  /**
   * Permet de trouver toutes les salles (rooms) de la base de donnée
   * @return array<Room> renvoit un tableau de Room
   */
  public function findAll()
  {
    $roomArray = [];
        $statement = $this->connection->prepare('SELECT * FROM room');
        $statement->execute();
        $results = $statement->fetchAll();
        foreach($results as $room){
            $roomArray[] = new Room($room['name'], $room['capacity'], $room['id']);
        }
        return $roomArray;
  } 

  /**
   * Permet de faire persister une nouvelle salle dans la base de données
   * @param Room $room la salle à faire persister
   * @return void
   */
  public function persist(Room $room) {
      $statement = $this->connection->prepare('INSERT INTO room (name,capacity) VALUES (:name, :capacity)');
      $statement->bindValue('name', $room->getName());
      $statement->bindValue('capacity', $room->getCapacity());
      $statement->execute();
      $room->setId($this->connection->lastInsertId());
  }

  /**
   * Permet de trouver une salle en fonction de son id
   * @param int $id l'id de la salle à trouver
   * @return Room|null la salle trouvée
   */
  public function findRoomById(int $id):?Room {
    $statement = $this->connection->prepare('SELECT * FROM room WHERE id=:id');
    $statement->bindValue('id', $id);
    $statement->execute();
    $result = $statement->fetch();
    if($result) {
        return new Room($result['name'],$result['capacity'], $result['id']);
    }
    return null;
  }

  /**
   * Permet de supprimer une salle de la base de données
   * @param int $id l'id de la salle à supprimer
   * @return void
   */
  public function deleteRoomById(int $id)
  {
    $statement = $this->connection->prepare('DELETE FROM room WHERE id=:id');
    $statement->bindValue('id', $id, PDO::PARAM_INT);
    $statement->execute();
  }

  /**
   * Permet de mettre à jour une salle de la base de données
   * @param string $name le nom de la salle
   * @param int $capacity la capacité de la salle
   * @param int $id l'id de la salle à mettre à jour
   * @return void
   */
  public function updateRoomById(string $name, int $capacity, int $id){
    $statement = $this->connection->prepare("UPDATE room SET name=:name, capacity=:capacity WHERE id=:id");
    $statement->bindValue('name', $name, PDO::PARAM_STR);
    $statement->bindValue('capacity', $capacity,PDO::PARAM_STR);
    $statement->bindValue('id', $id, PDO::PARAM_INT);
    $statement->execute();
  }


}


 


