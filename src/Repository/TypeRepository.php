<?php

namespace App\Repository;
use App\Entities\Type;

use PDO;


class TypeRepository {
    private PDO $connection;

    public function __construct()
    {
        $this->connection = new PDO('mysql:host=localhost;dbname=mjc_simplon', 'simplon', '1234');
    }
    /**
     * Méthode qui prend un ligne de résultat PDO et la convertit en instance 
     * de Profile
     * @param array $line la ligne de résultat à convertir, sous forme de tableau associatif
     * @return Type l'instance de profil
     */

     /**
      * Convertit le sql récupéré à partir de la base de donnée en classe php Type
      * @param array $line le tableau récupéré à partir de la base de données
      * @return Type le type de classe Type créé à partir du sql
      */
     public function sqlToType(array $line):Type{
        return new Type($line["type"], $line['id']);
    }

    /**
     * Renvoit tous les types de la table type de la base de données
     * @return Type[] tableau de Type
     */
    public function findAll()
    {
        $typeArray = [];
        $statement = $this->connection->prepare('SELECT * FROM type');
        $statement->execute();
        $results = $statement->fetchAll();
        
        foreach($results as $result){
            $type = $this->sqlToType($result);
            $type->setType($result["type"]);
            $typeArray[] = $type;
        }
        return $typeArray;
    }
    /**
     * Permet de faire persister un nouveau Type dans la base de donnée
     * @param Type $type le type à faire persister
     * @return void
     */
    public function persist(Type $type)
    {
        $statement = $this->connection->prepare('INSERT INTO type (type) VALUES (:type)');
        $statement->bindValue('type', $type->getType());
        $statement->execute();
        $type->setId($this->connection->lastInsertId());
    }

    /**
     * Permet de trouver un type à partir de son id
     * @param int $id l'id du type que l'on veut trouver
     * @return Type|null le type trouvé à partir de l'id
     */
    public function findById(int $id){
        $statement = $this->connection->prepare('SELECT * FROM type WHERE id=:id');
        $statement->bindValue('id', $id, PDO::PARAM_INT);
        $statement->execute();
        $result = $statement->fetch();

        if($result) {
            return new Type($result["type"], $result['id']);
        }
        return null;

    }

    /**
     * Permet de mettre à jour un type dans la base de données
     * @param Type $type le type que l'on veut mettre à jour
     * @return void
     */
    public function update(Type $type)
    {
        $statement = $this->connection->prepare('UPDATE type SET type=:type WHERE id=:id');
        $statement->bindValue('type', $type->getType(), PDO::PARAM_STR);
        $statement->bindValue('id', $type->getId(), PDO::PARAM_INT);
        $statement->execute();
    }

    /**
     * Permet de supprimer un type de la base de donnée
     * @param int $id l'id du type que l'on veut supprimer
     * @return void
     */
    public function delete(int $id)
    {
        $statement = $this->connection->prepare('DELETE FROM type WHERE id = :id');
        $statement->bindValue('id', $id, PDO::PARAM_INT);
        $statement->execute();
    }

    /**
     * ¨¨ermet de trouver toutes les activité d'un type
     * @param Type $type le type dont on veut trouver les activités
     * @return array renvoit un tableau d'activités
     */
    public function findActivitiesByType(int $id):array
    {
        $array = [];
        $statement = $this->connection->prepare('SELECT type.id, activity.name FROM type LEFT JOIN activity_type ON type.id=activity_type.id_type LEFT JOIN activity ON activity.id=activity_type.id_activity WHERE type.id=:id');

        $statement->bindValue('id', $id);
        $statement->execute();
        
        $results = $statement->fetchAll();
        foreach($results as $line){
            $activity = new Type($line["name"], $line['id']);
            $array[] = $activity;
        }
        return $array;
    }
    

}


