<?php

namespace App\Repository;
use App\Entities\Contact;
use App\Entities\Profile;
use App\Entities\User;
use App\Entities\Activity;
use DateTime;
use PDO;
 
class ProfileRepository {
    private PDO $connection;

    public function __construct()
    {
        $this->connection = new PDO('mysql:host=localhost;dbname=mjc_simplon', 'simplon', '1234');
    }

    /**
     * Méthode qui prend un ligne de résultat PDO et la convertit en instance 
     * de Profile
     * @param array $line la ligne de résultat à convertir, sous forme de tableau associatif
     * @return Profile l'instance de profil
     */

    public function sqlToProfile(array $line):Profile{
        return new Profile($line["name"], $line["firstname"], new DateTime($line["birthdate"]), $line["id"]);
    }

    /**
     * Faire une requête SQL vers la base de données et convertir les résultats de
     * cette requêtes en instances de la classe/entité Profile
     * @return Profile[]
     */

    public function findAll()
    {
        $profileArray = [];

        $statement = $this->connection->prepare('SELECT *, contact.id con_id, user.id us_id FROM profile LEFT JOIN contact ON contact.id = profile.id_contact LEFT JOIN user ON user.id = profile.id_user');
        
        $statement->execute();
        $results = $statement->fetchAll();
        
        foreach($results as $result){
            $profile = $this->sqlToProfile($result);
            $profile->setUser(new User($result["username"], $result["password"], $result["role"], $result["us_id"]));
            $profile->setContact(new Contact($result["mail"],  $result["phoneNumber"],  $result["ZIPCode"], $result["city"], $result["con_id"]));
            $profileArray[] = $profile;
        }
        return $profileArray;
    } 

    /**
     * Méthode qui effectue une requête SQL vers la base de donnée pour trouve un profil spécifique à partir de son id et qui le renvoit comme instance de la classe Profile.
     * @param int $id L'id du profil à récupérer
     * @return Profile le profil récupéré, instance de la classe Profile
     */
    public function findById(int $id){

        $statement = $this->connection->prepare('SELECT *, contact.id con_id, user.id us_id FROM profile LEFT JOIN contact ON contact.id = profile.id_contact LEFT JOIN user ON user.id = profile.id_user WHERE profile.id = :id');

        $statement->bindValue('id', $id, PDO::PARAM_INT);
        $statement->execute();
        $results = $statement->fetch();

        $profile = $this->sqlToProfile($results);

        $profile->setUser(new User($results["username"], $results["password"], $results["role"], $results["us_id"]));

        $profile->setContact(new Contact($results["mail"],  $results["phoneNumber"],  $results["ZIPCode"], $results["city"], $results["con_id"]));        
        return $profile;
    }

    /**
     * Méthode qui permet de supprimer un profil de la base de donnée à partir de son id
     * @param int $id, l'id du profil à supprimer
     * @return void
     */
    public function delete(int $id){
        $statement = $this->connection->prepare('DELETE FROM profile WHERE id = :id');
        $statement->bindValue('id', $id, PDO::PARAM_INT);
        $statement->execute();
    }

        /**
     * Méthode permettant de faire persister une instance de Profile sur la base de données
     * @param Profile $profile le profil à faire persister
     * @return void Aucun retour, mais une fois persisté, le profil aura un id assigné
     */
    
    public function persist(Profile $profile){

        $statement = $this->connection->prepare('INSERT INTO profile (name, firstname, birthdate) VALUES (:name,:firstname, :birthdate)');
        $statement->bindValue('name', $profile->getName(), PDO::PARAM_STR);
        $statement->bindValue('firstname', $profile->getFirstname(), PDO::PARAM_STR);
        $statement->bindValue('birthdate', $profile->getBirthdate()->format('Y-m-d'), PDO::PARAM_STR);
        $statement->execute();
        $profile->setId($this->connection->lastInsertId());
    }

    /**
     * Permet de modifier un profil dans la base de donnée en lui fournissant une nouvelle instance de Profile avec le bon id
     * @param Profile $profile le nouveau profil que l'on veut
     * @return void
     */
    public function update(Profile $profile){

        $statement = $this->connection->prepare('UPDATE profile SET name = :name, firstname = :firstname, birthdate = :birthdate WHERE id = :id');

        $statement->bindValue('name', $profile->getName(), PDO::PARAM_STR);
        $statement->bindValue('firstname', $profile->getFirstname(), PDO::PARAM_STR);
        $statement->bindValue('birthdate', $profile->getBirthdate()->format('Y-m-d'), PDO::PARAM_STR);
        $statement->bindValue('id', $profile->getId(), PDO::PARAM_INT);
        $statement->execute();
    }


}