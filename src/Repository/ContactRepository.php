<?php

namespace App\Repository;
use App\Entities\Contact;
use PDO;
 
class ContactRepository {

    private PDO $connection;

    public function __construct()
    {
        $this->connection = new PDO('mysql:host=localhost;dbname=mjc_simplon', 'simplon', '1234');
    }

    /**
     * Méthode qui prend un ligne de résultat PDO et la convertit en instance 
     * de Contact
     * @param array $line la ligne de résultat à convertir, sous forme de tableau associatif
     * @return Contact l'instance de profil
     */

     public function sqlToContact(array $line):Contact{
        return new Contact($line["mail"], $line["phoneNumber"], $line["street"], $line["ZIPCode"], $line["city"], $line["id"]);
    }

    /**
     * Faire une requête SQL vers la base de données et convertir les résultats de
     * cette requêtes en instances de la classe/entité Contact
     * @return Contact[]
     */

     public function findAll()
     {
         $contactArray = [];
 
         $statement = $this->connection->prepare('SELECT * FROM contact');
         
         $statement->execute();
         
         $results = $statement->fetchAll();
         
         foreach($results as $contact){
             $contactArray[] = $this->sqlToContact($contact);
         }
         return $contactArray;
     } 

     /**
     * Méthode qui effectue une requête SQL vers la base de donnée pour trouve un contact spécifique à partir de son id et qui le renvoit comme instance de la classe Contact.
     * @param int $id L'id du contact à récupérer
     * @return Contact le contact récupéré, instance de la classe Contact
     */
     public function findById(int $id){

        $statement = $this->connection->prepare('SELECT * FROM contact WHERE id = :id');

        $statement->bindValue('id', $id, PDO::PARAM_INT);

        $statement->execute();

        $results = $statement->fetch();

        $contact = $this->sqlToContact($results);
      
        return $contact;
    }

    /**
     * Méthode qui permet de supprimer un contact de la base de donnée à partir de son id
     * @param int $id, l'id du contact à supprimer
     * @return void
     */
    public function delete(int $id){
        $statement = $this->connection->prepare('DELETE FROM contact WHERE id = :id');

        $statement->bindValue('id', $id, PDO::PARAM_INT);

        $statement->execute();
    }

            /**
     * Méthode permettant de faire persister une instance de Contact sur la base de données
     * @param Contact $profile le contact à faire persister
     * @return void Aucun retour, mais une fois persisté, le contact aura un id assigné
     */
    
     public function persist(Contact $contact){

        $statement = $this->connection->prepare('INSERT INTO contact (mail, phoneNumber, street, ZIPCode, city) VALUES (:mail,:phoneNumber, :street, :ZIPCode, :city)');

        $statement->bindValue('mail', $contact->getMail(), PDO::PARAM_STR);

        $statement->bindValue('phoneNumber', $contact->getPhoneNumber(), PDO::PARAM_STR);
        
        $statement->bindValue('street', $contact->getStreet(), PDO::PARAM_STR);

        $statement->bindValue('ZIPCode', $contact->getZIPCode(), PDO::PARAM_STR);

        $statement->bindValue('city', $contact->getCity(), PDO::PARAM_STR);

        $statement->execute();

        $contact->setId($this->connection->lastInsertId());
    }

        /**
     * Permet de modifier un contact dans la base de donnée en lui fournissant une nouvelle instance de Contact avec le bon id
     * @param Contact $contact le nouveau contact que l'on veut
     * @return void
     */
    public function update(Contact $contact){

        $statement = $this->connection->prepare('UPDATE contact SET mail = :mail, phoneNumber = :phoneNumber, street = :street, ZIPCode = :ZIPCode, city = :city WHERE id = :id');

        $statement->bindValue('mail', $contact->getMail(), PDO::PARAM_STR);

        $statement->bindValue('phoneNumber', $contact->getPhoneNumber(), PDO::PARAM_STR);
        
        $statement->bindValue('street', $contact->getStreet(), PDO::PARAM_STR);

        $statement->bindValue('city', $contact->getCity(), PDO::PARAM_STR);

        $statement->bindValue('ZIPCode', $contact->getZIPCode(), PDO::PARAM_STR);

        $statement->bindValue('id', $contact->getId(), PDO::PARAM_INT);

        $statement->execute();
    }
}