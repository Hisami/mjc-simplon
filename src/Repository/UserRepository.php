<?php

namespace App\Repository;
use App\Entities\Contact;
use App\Entities\Profile;
use App\Entities\User;
use DateTime;
use PDO;


class UserRepository{
    private PDO $connection;
    public function __construct() {
        $this->connection = new PDO('mysql:host=localhost;dbname=mjc_simplon', 'simplon', '1234');
    }

    /**
     * Méthode qui prend un ligne de résultat PDO et la convertit en instance 
     * de User
     * @param array $line la ligne de résultat à convertir, sous forme de tableau associatif
     * @return User l'instance de profil
     */

     public function sqlToUser(array $line):User{
        return new User($line["username"], $line["password"], $line["role"], $line["id"]);
    }

    /**
     * Faire une requête SQL vers la base de données et convertir les résultats de
     * cette requêtes en instances de la classe/entité User
     * @return User[]
     */

     public function findAll()
     {
         $array = [];
 
         $statement = $this->connection->prepare('SELECT * FROM user');
         
         $statement->execute();
         
         $results = $statement->fetchAll();
         
         foreach($results as $user){
             $array[] = $this->sqlToUser($user);
         }
         return $array;
     } 
    
 /**
     * Méthode qui effectue une requête SQL vers la base de donnée pour trouve un user spécifique à partir de son id et qui le renvoit comme instance de la classe User.
     * @param int $id L'id du user à récupérer
     * @return User le user récupéré, instance de la classe User
     */
    public function findById(int $id){

        $statement = $this->connection->prepare('SELECT * FROM user WHERE id = :id');

        $statement->bindValue('id', $id, PDO::PARAM_INT);

        $statement->execute();

        $results = $statement->fetch();

        $contact = $this->sqlToUser($results);
      
        return $contact;
    }

    /**
     * Méthode qui permet de supprimer un user de la base de donnée à partir de son id
     * @param int $id, l'id du user à supprimer
     * @return void
     */
    public function delete(int $id){

        $this->connection->exec("SET foreign_key_checks = 0");

        $statement = $this->connection->prepare('DELETE FROM user WHERE id = :id');

        $statement->bindValue('id', $id, PDO::PARAM_INT);

        $statement->execute();

        $this->connection->exec("SET foreign_key_checks = 1");
    }

                /**
     * Méthode permettant de faire persister une instance de Contact sur la base de données
     * @param User $profile le contact à faire persister
     * @return void Aucun retour, mais une fois persisté, le contact aura un id assigné
     */
    
     public function persist(User $user){

        $statement = $this->connection->prepare('INSERT INTO user (username, password, role) VALUES (:username, :password, :role)');

        $statement->bindValue('username', $user->getUsername(), PDO::PARAM_STR);

        $statement->bindValue('password', $user->getPassword(), PDO::PARAM_STR);
        
        $statement->bindValue('role', $user->getRole(), PDO::PARAM_STR);

        $statement->execute();

        $user->setId($this->connection->lastInsertId());
    }

            /**
     * Permet de modifier un user dans la base de donnée en lui fournissant une nouvelle instance de User avec le bon id
     * @param User $user le nouveau user que l'on veut
     * @return void
     */
    public function update(User $user){

        $statement = $this->connection->prepare('UPDATE user SET username = :username, password = :password, role = :role WHERE id = :id');

        $statement->bindValue('username', $user->getUsername(), PDO::PARAM_STR);

        $statement->bindValue('password', $user->getPassword(), PDO::PARAM_STR);
        
        $statement->bindValue('role', $user->getRole(), PDO::PARAM_STR);

        $statement->bindValue('id', $user->getId(), PDO::PARAM_INT);

        $statement->execute();
    }

        /**
     * Permet d'afficher tous les profils d'un user
     * @param int $id l'id du user dont on veut trouver les profils
     * @return array tableau de profils
     */
    public function findProfilesByUser(User $user):array{
        $array = [];

        $statement = $this->connection->prepare('SELECT *, contact.id con_id, user.id us_id FROM profile LEFT JOIN contact ON contact.id = profile.id_contact LEFT JOIN user ON user.id = profile.id_user WHERE user.id = :id');

        $statement->bindValue('id', $user->getId(), PDO::PARAM_INT);

        $statement->execute();

        $results = $statement->fetchAll();

        foreach($results as $result){
            $profile = new Profile($result["name"], $result["firstname"], new DateTime($result["birthdate"]), $result["id"]);
            $profile->setUser($user);
            $profile->setContact(new Contact($result["mail"],  $result["phoneNumber"],  $result["ZIPCode"], $result["city"], $result["con_id"]));
            $array[] = $profile;
        }

        return $array;
    }
}