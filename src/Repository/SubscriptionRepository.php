<?php

namespace App\Repository;
use App\Entities\Subscription;
use App\Entities\Profile;
use App\Entities\Activity;
use PDO;
use DateTime;

class SubscriptionRepository
{
  private PDO $connection;

  public function __construct()
  {
    $this->connection = new PDO('mysql:host=localhost;dbname=mjc_simplon', 'simplon', '1234');
  }

/**
     * Méthode qui prend un ligne de résultat PDO et la convertit en instance 
     * de Subscription
     * @param array $line la ligne de résultat à convertir, sous forme de tableau associatif
     * @return Subscription l'instance de subscription
     */
  private function sqlToSubscription(array $line): Subscription
  {
    return new Subscription(new Datetime($line["date"]),$line['pending'], $line["sub_id"]);
  }

/**
     * Méthode qui prend un ligne de résultat PDO et la convertit en instance 
     * de Profile
     * @param array $line la ligne de résultat à convertir, sous forme de tableau associatif
     * @return Profile l'instance de profile
     */
  private function sqlToProfile(array $line): Profile
  {
    return new Profile($line["pro_name"], $line["firstname"], new DateTime($line["birthdate"]), $line["pro_id"]);
  }

/**
     * Méthode qui prend un ligne de résultat PDO et la convertit en instance 
     * de Profile
     * @param array $line la ligne de résultat à convertir, sous forme de tableau associatif
     * @return Activity l'instance de activity
     */
  private function sqlToActivity(array $line): Activity
  {
    return new Activity($line["ac_name"], new DateTime($line["dateStart"]), new DateTime($line["dateEnd"]), $line["frequency"], $line["nbOfSession"], $line["ageMin"], $line["ageMax"], $line["price"], $line["maxStudent"], $line["pitch"], $line["level"], $line["img"], $line["ac_id"]);
  }

/**
     * Faire une requête SQL vers la base de données et convertir les résultats de
     * cette requêtes en instances de la classe/entité Subscription
     * @return Subscription[]
     */

  public function findAll()
  {
    $subscriptionArray = [];
    $statement = $this->connection->prepare('SELECT *, subscription.id sub_id, profile.name pro_name,profile.id pro_id, activity.name ac_name,activity.id ac_id, profile.id pro_id FROM subscription 
    LEFT JOIN activity ON subscription.id_activity = activity.id
    LEFT JOIN profile ON subscription.id_profile = profile.id');
    $statement->execute();
    $results = $statement->fetchAll();
    foreach ($results as $line) {
      $newsubscription = $this->sqlToSubscription($line);
      $newsubscription->setProfile($this->sqlToProfile($line));
      $newsubscription->setActivity($this->sqlToActivity($line));
      $subscriptionArray[] = $newsubscription;
    }
    return $subscriptionArray;
  }

/**
     * Méthode qui effectue une requête SQL vers la base de donnée pour trouve un profil spécifique à partir de son id et qui le renvoit comme instance de la classe Subscription.
     * @param int $id L'id du subscription à récupérer
     * @return Subscription le profil récupéré, instance de la classe Subscription
     */
  public function findById(int $id): ?Subscription
  {
    $statement = $this->connection->prepare('SELECT *, subscription.id sub_id, profile.name pro_name,profile.id pro_id, activity.name ac_name,activity.id ac_id, profile.id pro_id FROM subscription 
    LEFT JOIN activity ON subscription.id_activity = activity.id
    LEFT JOIN profile ON subscription.id_profile = profile.id  WHERE subscription.id=:id');
    $statement->bindValue('id', $id);
    $statement->execute();
    $result = $statement->fetch();
    if ($result) {
      $subscription = $this->sqlToSubscription($result);
      $subscription->setProfile($this->sqlToProfile($result));
      $subscription->setActivity($this->sqlToActivity($result));
    }
    return $subscription;
  }

/**
     * Méthode permettant de faire persister une instance de Subscription sur la base de données
     * @param Subscription $subscription subscription à faire persister
     * @return void Aucun retour, mais une fois persisté, le subscription aura un id assigné
     */
  public function persist(Subscription $subscription)
  {
    $statement = $this->connection->prepare('INSERT INTO subscription (date,pending,id_profile,id_activity) VALUES (:date,:pending,:id_profile,:id_activity)');
    $statement->bindValue('date', $subscription->getDate()->format('Y-m-d'));
    $statement->bindValue('pending', $subscription->getPending());
    $statement->bindValue('id_profile', $subscription->getProfile()->getId());
    $statement->bindValue('id_activity', $subscription->getActivity()->getId());

    $statement->execute();

    $subscription->setId($this->connection->lastInsertId());
  }

/**
     * Méthode qui effectue une requête SQL vers la base de donnée pour trouve un subscription spécifique à partir de l'id d'un Activity
     * @param int $id L'id de l'activity à récupérer
     * @return Subscription le subscription récupéré, instance de la classe Subscription
     */
  public function findByActivityId(int $id): ?Subscription
  {
    $statement = $this->connection->prepare('SELECT *, subscription.id sub_id, profile.name pro_name,profile.id pro_id, activity.name ac_name,activity.id ac_id, profile.id pro_id FROM subscription 
    LEFT JOIN activity ON subscription.id_activity = activity.id
    LEFT JOIN profile ON subscription.id_profile = profile.id  WHERE activity.id=:id');
    $statement->bindValue('id', $id);
    $statement->execute();
    $result = $statement->fetch();
    if ($result) {
      $subscription = $this->sqlToSubscription($result);
      $subscription->setProfile($this->sqlToProfile($result));
      $subscription->setActivity($this->sqlToActivity($result));
    }
    return $subscription;

  }

/**
     * Méthode qui permet de supprimer un subscription de la base de donnée à partir de son id
     * @param int $id, l'id du subscription à supprimer
     * @return $result
     */
  public function delete(int $id)
  {   
      $statement = $this->connection->prepare("DELETE subscription FROM 
      subscription LEFT JOIN activity ON subscription.id_activity = activity.id
      LEFT JOIN profile ON subscription.id_profile = profile.id WHERE subscription.id=:id;");
      $statement->bindValue(":id", $id, PDO::PARAM_INT);
      $result= $statement->execute();

    return $result;
  }


/**
     * Permet de modifier un subscription dans la base de donnée en lui fournissant une nouvelle instance de Subscription avec le bon id
     * @param Subscription $subscription le nouveau subscription que l'on veut
     * @return void
     */
    public function update(Subscription $subscription){

      $statement = $this->connection->prepare('UPDATE subscription SET date=:date,pending=:pending,id_profile=:id_profile, id_activity =:id_activity WHERE subscription.id=:id;
      ');
      $statement->bindValue('date', $subscription->getDate()->format('Y-m-d'), PDO::PARAM_STR);
      $statement->bindValue('pending', $subscription->getPending(), PDO::PARAM_INT);
      $statement->bindValue('id_profile', $subscription->getProfile()->getId(), PDO::PARAM_STR);
      $statement->bindValue('id_activity', $subscription->getActivity()->getId(), PDO::PARAM_STR);
      $statement->bindValue('id', $subscription->getId(), PDO::PARAM_INT);

      $statement->execute();
  }

/**
     * Permet de modifier le pending (true or false)d'un subscription dans la base de donnée en lui fournissant une nouvelle instance de Subscription avec le bon id
     * @param Subscription $subscription 
     * @return void
     */
  public function updatePending(Subscription $subscription): void
  {
    $subscription->changePending();
    $statement=$this->connection->prepare('UPDATE subscription SET pending=:pending WHERE subscription.id= :id');
    $statement->bindValue('pending', $subscription->getPending(), PDO::PARAM_INT); 
    $statement->bindValue('id', $subscription->getId(), PDO::PARAM_INT); 
    $statement->execute();  
    }

/**
     * Permet d'afficher toutes les subscriptions d'un Profile
     * @param Profile $profile l'instance du profile dont on veut trouver les subscriptions
     * @return array tableau de subscriptions
     */
    public function findByProfile(Profile $profile){
      $subscriptionArray = [];
      $statement = $this->connection->prepare('SELECT *, subscription.id sub_id, profile.name pro_name,profile.id pro_id, activity.name ac_name,activity.id ac_id, profile.id pro_id FROM subscription 
      LEFT JOIN activity ON subscription.id_activity = activity.id
      LEFT JOIN profile ON subscription.id_profile = profile.id WHERE id_profile= :profile_id');
      $statement->bindValue('profile_id',$profile->getId(),PDO::PARAM_INT );
      $statement->execute();
      $results = $statement->fetchAll();
      foreach ($results as $line) {
        $newsubscription = $this->sqlToSubscription($line);
        /* $newsubscription->setProfile($this->sqlToProfile($line)); */
        $newsubscription->setActivity($this->sqlToActivity($line));
        $subscriptionArray[] = $newsubscription;
      }
      return $subscriptionArray;
    }
    
/**
     * Permet d'afficher toutes les profiles dont le pending est false
     * @return array tableau de profiles 
     */
    public function findProfileWithPendingfalse(){
      $profilesArray = [];
      $statement = $this->connection->prepare('SELECT  *, subscription.id sub_id, profile.name pro_name,profile.id pro_id, activity.name ac_name,activity.id ac_id, profile.id pro_id FROM subscription 
      LEFT JOIN activity ON subscription.id_activity = activity.id
      LEFT JOIN profile ON subscription.id_profile = profile.id GROUP BY profile.id HAVING pending=0 ');
      $statement->execute();
      $results = $statement->fetchAll();
      foreach ($results as $line) {
      $profilesArray[] = $this->sqlToProfile($line);
      }
      return $profilesArray;
    }

}



